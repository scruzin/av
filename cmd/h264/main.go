// Read a H.264 file in YUV 420 progressive format and output it as
// H.264 in bytestream format.
//
// Each macroblock in our 4:2:0 sampling scheme contains 16 x 16 luma
// samples, and two 8 x 8 blocks of chroma samples. For this simple
// encoder, there is no video data compression and samples are
// directly copied from the input into the output.

// There are three NAL units we have to emit:
//   Sequence Parameter Set (SPS): Once per stream
//   Picture Parameter Set (PPS): Once per stream
//   Slice Header: Once per video frame
//     Slice Header information
//     Macroblock Header: Once per macroblock
//     Coded Macroblock Data: The actual coded video for the macroblock
//
// Each NAL unit is preceded by the H.264 Annex B start code.
//
// To run this program:
//  h264 [-i in.yuv] [-o out.h264]
//
// To generate suitable H.264 YUV input for this program:
//  ffmpeg -i in.mov -s sqcif -pix_fmt yuv420p in.yuv
//
// To view stats on the input file:
//  ffprobe -show_streams -count_frames -pretty in.yuv
//
// This program is originally based on https://cardinalpeak.com/blog/worlds-smallest-h-264-encoder.
// The latter is Copyright Cardinal Peak, LLC.  http://cardinalpeak.com

package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"unsafe"
)

// SQCIF is 128 pixels x 96 pixels video resolution
const (
	lumaWidth    = 128
	lumaHeight   = 96
	chromaWidth  = lumaWidth / 2
	chromaHeight = lumaHeight / 2
	lumaSize     = lumaHeight * lumaWidth
	chromaSize   = chromaHeight * chromaWidth
)

// H.264 YUV data format (as written by ffmpeg -s sqcif -pix_fmt yuv420p).
type H264YUVFrame struct {
	Y  [lumaHeight][lumaWidth]byte
	Cb [chromaHeight][chromaWidth]byte
	Cr [chromaHeight][chromaWidth]byte
}

// Our H.264 frame representation.
// NB: We use single-dimensional slices to avoid copying data.
type H264Frame struct {
	Y  []byte // of lumaSize
	Cb []byte // of chromaSize
	Cr []byte // of chromaSize
}

// H.264 bitstream constants.
var (
	start            = []byte{0x00, 0x00, 0x00, 0x01}
	sps              = []byte{0x00, 0x00, 0x00, 0x01, 0x67, 0x42, 0x00, 0x0a, 0xf8, 0x41, 0xa2}
	pps              = []byte{0x00, 0x00, 0x00, 0x01, 0x68, 0xce, 0x38, 0x80}
	sliceHeader      = []byte{0x00, 0x00, 0x00, 0x01, 0x05, 0x88, 0x84, 0x21, 0xa0}
	stop             = []byte{0x80}
	macroblockHeader = []byte{0x0d, 0x00}
)

var (
	infile  = flag.String("i", "in.yuv", "Input file")
	outfile = flag.String("o", "out.h264", "Output file")
)

// main reads from the input file and writes PPS, SPS and I slices to the output file.
func main() {
	flag.Parse()

	var yf H264YUVFrame
	var frameSize = int(unsafe.Sizeof(yf))
	fmt.Printf("Reading from %s, expecing frames of size %d\n", *infile, frameSize)

	input, err := ioutil.ReadFile(*infile)
	if err != nil {
		log.Fatalf("Error opening %s: %v", *infile, err)
	}

	var buf bytes.Buffer

	// Write SPS NAL unit.
	_, err = buf.Write(start)
	if err != nil {
		log.Fatalf("Error writing start code: %v", err)
	}
	_, err = buf.Write(sps)
	if err != nil {
		log.Fatalf("Error writing SPS: %v", err)
	}

	// Write PPS NAL unit.
	_, err = buf.Write(start)
	if err != nil {
		log.Fatalf("Error writing start code: %v", err)
	}
	_, err = buf.Write(pps)
	if err != nil {
		log.Fatalf("Error writing PPS: %v", err)
	}

	frames := 0
	macroblocks := 0
	for len(input) > 0 {
		var fr H264Frame

		// Pointer conversions to avoid copying. Ugly but fast.
		bytes := input[:lumaSize]
		fr.Y = *(*[]byte)(unsafe.Pointer(&bytes))
		input = input[lumaSize:]
		bytes = input[:chromaSize]
		fr.Cb = *(*[]byte)(unsafe.Pointer(&bytes))
		input = input[chromaSize:]
		bytes = input[:chromaSize]
		fr.Cr = *(*[]byte)(unsafe.Pointer(&bytes))
		input = input[chromaSize:]

		// Write slice header NAL unit.
		_, err = buf.Write(start)
		if err != nil {
			log.Fatalf("Error writing start code: %v", err)
		}
		_, err = buf.Write(sliceHeader)
		if err != nil {
			log.Fatalf("Error writing slice header: %v", err)
		}

		for i := 0; i < lumaHeight/16; i++ {
			for j := 0; j < lumaWidth/16; j++ {
				err = fr.macroblock(&buf, i, j)
				if err != nil {
					log.Fatalf("Error writing macroblock(%d, %d): %v", i, j, err)
				}
				macroblocks += 1
			}
		}

		_, err = buf.Write(stop)
		if err != nil {
			log.Fatalf("Error writing stop: %v", err)
		}
		frames += 1
	}

	// Output the video.
	f, err := os.Create(*outfile)
	if err != nil {
		log.Fatalf("Error creating %s: %v", *outfile, err)
	}
	_, err = buf.WriteTo(f)
	if err != nil {
		log.Fatalf("Error writing buffer: %v", err)
	}
	f.Close()

	fmt.Printf("Wrote %d frames, %d macroblocks to %s\n", frames, macroblocks, *outfile)
}

// macroblock writes a macroblock's worth of YUV data in I_PCM mode.
// NB: We deliberately write bytes one at a time for illustration purposes.
func (fr *H264Frame) macroblock(buf *bytes.Buffer, i, j int) error {
	// Write a macro block header unless the very start of a frame.
	if i > 0 || j > 0 {
		_, err := buf.Write(macroblockHeader)
		if err != nil {
			return err
		}
	}

	// Write Y data.
	for x := i * 16; x < (i+1)*16; x++ {
		for y := j * 16; y < (j+1)*16; y++ {
			err := buf.WriteByte(fr.Y[x*lumaWidth+y])
			if err != nil {
				return err
			}
		}
	}

	// Write Cb data.
	for x := i * 8; x < (i+1)*8; x++ {
		for y := j * 8; y < (j+1)*8; y++ {
			err := buf.WriteByte(fr.Cb[x*chromaWidth+y])
			if err != nil {
				return err
			}
		}
	}

	// Write Cr data.
	for x := i * 8; x < (i+1)*8; x++ {
		for y := j * 8; y < (j+1)*8; y++ {
			err := buf.WriteByte(fr.Cr[x*chromaWidth+y])
			if err != nil {
				return err
			}
		}
	}
	return nil
}
