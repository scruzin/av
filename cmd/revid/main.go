/*
NAME
  revid - a testbed for re-muxing and re-directing video streams as MPEG-TS over various protocols.

DESCRIPTION
  See Readme.md

USAGE
  Stream a MPEG-TS file to a local dev_appserver over HTTP:
  revid -i file.ts -m h -o "http://127.0.0.1:8080/recv?ma=00:00:00:00:00:01&dk=1&V0="

  Rudimentary support has been added for capturing and redirecting audio in (raw) PCM format.
  Instead of supplying a file name or RTSP URL as the input, supply a URL of the form audio:xxx
  Note that there is currently no support for remuxing audio.

  Play audio files as:
  aplay -t raw -r 42100 -f S32_LE -c 2 /tmp/revid.xxx
  or with vlc:
  vlc --demux=rawaud --rawaud-fourcc=s32l --rawaud-channels 2 --rawaud-samplerate 44100 /tmp/revid.xxx

AUTHOR
  Alan Noble <anoble@gmail.com>

LICENSE
  revid is Copyright (C) 2017-2018 Alan Noble.

  It is free software: you can redistribute it and/or modify them
  under the terms of the GNU General Public License as published by the
  Free Software Foundation, either version 3 of the License, or (at your
  option) any later version.

  It is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License
  along with revid in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
*/

// revid is a testbed for re-muxing and re-directing audio/video streams as MPEG-TS over various protocols.
package main

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"time"

	"github.com/Comcast/gots/packet"
	"github.com/Comcast/gots/packet/adaptationfield"
	"github.com/Comcast/gots/psi"
	"github.com/yobert/alsa"
)

// defaults and networking consts
const (
	defaultPID        = 256
	defaultHTTPOutput = "http://localhost:8080?"
	defaultUDPOutput  = "udp://0.0.0.0:16384"
	defaultRTPOutput  = "rtp://0.0.0.0:16384"
	mp2tPacketSize    = 188  // MPEG-TS packet size
	mp2tMaxPackets    = 2016 // # first multiple of 7 and 8 greater than 2000
	udpPackets        = 7    // # of UDP packets per ethernet frame (8 is the max)
	rtpPackets        = 7    // # of RTP packets per ethernet frame (7 is the max)
	rtpHeaderSize     = 12
	rtpSSRC           = 1 // any value will do
	ffmpegPath        = "/usr/bin/ffmpeg"
	audioChannels     = 2
)

// flag values
const (
	filterFixPTS        = 0x0001
	filterDropAudio     = 0x0002
	filterScale640      = 0x0004
	filterScale320      = 0x0008
	filterFixContinuity = 0x0010
	dumpProgramInfo     = 0x0100 // 256
	dumpPacketStats     = 0x0200 // 512
	dumpPacketHeader    = 0x0400 // 1024
	dumpPacketPayload   = 0x0800 // 2048
)

// globals
var (
	inputFile       = false
	readWrite       = readWriteVideo
	sendClip        = sendClipToHTTP
	mimeType        = "video/mp2t"
	packetsPerFrame = rtpPackets
	clipCount       int
	expectCC        int
	dumpCC          int
	dumpPCRBase     uint64
	rtpSequenceNum  uint16
)

// command-line flags
var (
	input       = flag.String("i", "", "Input RTSP URL, audio URL or file")
	output      = flag.String("o", "", "Output URL (HTTP, UDP or RTP)")
	mode        = flag.String("m", "r", "Mode: one of f,h,u,r or d")
	flags       = flag.Int("f", 0, "Flags: see readme for explanation")
	frameRate   = flag.Int("r", 0, "Frame rate (25fps by default for video or 44100Hz for audio)")
	selectedPID = flag.Int("p", defaultPID, "Select packets with this packet ID (PID)")
)

func main() {
	flag.Parse()

	if *input == "" {
		log.Fatal("Input (-i) required\n")
	} else if strings.HasPrefix(*input, "rtsp:") {
		if *frameRate == 0 {
			*frameRate = 25 // fps
		}
	} else if strings.HasPrefix(*input, "audio:") {
		if *frameRate == 0 {
			*frameRate = 44100 // Hz
		}
		if runtime.GOOS == "windows" {
			fmt.Fprintln(os.Stderr, "Audio not supported on Windows")
			os.Exit(1)
		}
		readWrite = readWriteAudio
		mimeType = "audio/x=pcm;format=s32l;channels=2;rate=" + strconv.Itoa(*frameRate)
	} else {
		inputFile = true
	}

	switch *mode {
	case "f":
		sendClip = sendClipToFile
	case "h":
		sendClip = sendClipToHTTP
		if *output == "" {
			*output = defaultHTTPOutput
		}
	case "u":
		sendClip = sendClipToUDP
		packetsPerFrame = udpPackets
		if *output == "" {
			*output = defaultUDPOutput
		}
	case "r":
		sendClip = sendClipToRTP
		packetsPerFrame = rtpPackets
		if *output == "" {
			*output = defaultRTPOutput
		}
	case "d":
		sendClip = sendClipToStdout
	default:
		log.Fatalf("Invalid mode %s\n", *mode)
	}

	if *flags&filterFixContinuity != 0 && *flags&dumpProgramInfo != 0 {
		log.Fatal("Cannot combine filterFixContinuity and dumpProgramInfo flags\n")
	}

	for {
		err := readWrite(*input, *output)
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		fmt.Fprintln(os.Stderr, "Restarting in 10s")
		time.Sleep(10 * time.Second)
	}
}

// readWriteVideo reads video from a file or an RTSP stream (specified by input) and
// rewrites the video in various formats and/or different protocols (HTTP, UDP or RTP).
func readWriteVideo(input string, output string) error {
	fmt.Printf("Capturing video from %s\n", input)

	// open the file or RTSP stream
	var rc io.ReadCloser
	var err error
	if inputFile {
		rc, err = os.Open(input)
	} else {
		rc, err = openRtsp(input)
	}
	if err != nil {
		return err
	}

	// (re)initialize globals
	clipCount = 0
	expectCC = -1
	dumpCC = -1
	dumpPCRBase = 0
	rtpSequenceNum = uint16(rand.Intn(1 << 15))

	// for UDP and RTP only dial once
	var conn net.Conn
	if strings.HasPrefix(output, "udp://") || strings.HasPrefix(output, "rtp://") {
		conn, err = net.Dial("udp", output[6:])
		if err != nil {
			return fmt.Errorf("Error dialing %s: %s", output, err)
		}
		defer conn.Close()
	}

	br := bufio.NewReader(rc)
	buf := make([]byte, mp2tPacketSize)
	clip := make([]byte, mp2tMaxPackets*mp2tPacketSize)
	clipSize := 0
	packetCount := 0
	now := time.Now()
	prevTime := now
	fmt.Printf("Looping\n")

	for {
		_, err := io.ReadFull(br, buf)
		if err != nil {
			if err == io.EOF {
				fmt.Printf("EOF encountered\n")
				break
			}
			return err
		}

		pkt, err := packet.FromBytes(buf)
		err = pkt.CheckErrors()
		if err != nil {
			return fmt.Errorf("Invalid packet: %s", err)
		}

		if *flags&filterFixContinuity != 0 && mp2tFixContinuity(pkt, packetCount, uint16(*selectedPID)) {
			fmt.Printf("Packet #%d.%d fixed\n", clipCount, packetCount)
		}

		copy(clip[clipSize:], buf)
		packetCount++
		clipSize += mp2tPacketSize

		// send if (1) our buffer is full or (2) 1 second has elapsed and we have % packetsPerFrame
		now = time.Now()
		if (packetCount == mp2tMaxPackets) ||
			(now.Sub(prevTime) > 1*time.Second && packetCount%packetsPerFrame == 0) {
			clipCount++
			if err = sendClip(clip[:clipSize], output, conn); err != nil {
				return err
			}
			clipSize = 0
			packetCount = 0
			prevTime = now
		}
	}
	rc.Close()
	return nil
}

// openRtsp "opens" an RTSP stream using ffmpeg and returns a pipe to the stream
func openRtsp(input string) (io.ReadCloser, error) {
	args := []string{
		"-r", strconv.Itoa(*frameRate),
		"-i", input,
	}

	if *flags&(filterFixPTS|filterScale640|filterScale320) == 0 {
		args = append(args, "-vcodec", "copy")
	} else {
		vfArg := []string{}

		if *flags&filterFixPTS != 0 {
			vfArg = append(vfArg, "setpts='PTS-STARTPTS'") // start counting PTS from zero
		}
		if *flags&filterScale640 != 0 {
			vfArg = append(vfArg, "scale=640:352")
		} else if *flags&filterScale320 != 0 {
			vfArg = append(vfArg, "scale=320:176")
		}
		args = append(args, "-vf", strings.Join(vfArg, ","))

	}

	if *flags&filterDropAudio == 0 {
		args = append(args, "-acodec", "copy")
	} else {
		args = append(args, "-an")
	}
	args = append(args, "-f", "mpegts", "-")

	fmt.Printf("Executing: %s %s\n", ffmpegPath, strings.Join(args, " "))
	cmd := exec.Command(ffmpegPath, args...)
	pipe, err := cmd.StdoutPipe()
	if err != nil {
		return nil, fmt.Errorf("Error creating pipe: %s", err)
	}
	err = cmd.Start()
	if err != nil {
		return nil, fmt.Errorf("Error starting pipe: %s", err)
	}
	return pipe, nil
}

// readWriteAudio inputs audio and writes it to the specified output in 60-second clips
// ToDo: add audio input device selection based on the supplied input string
func readWriteAudio(input string, output string) error {
	fmt.Printf("Capturing audio from %s\n", input)

	// prepare the device
	dev, err := getRecordingDevice()
	if err != nil {
		return err
	}
	if err = dev.Open(); err != nil {
		return err
	}
	defer dev.Close()

	channels, err := dev.NegotiateChannels(audioChannels)
	if err != nil {
		return err
	}

	rate, err := dev.NegotiateRate(*frameRate)
	if err != nil {
		return err
	}

	format, err := dev.NegotiateFormat(alsa.S32_LE)
	if err != nil {
		return err
	}

	bufferSize, err := dev.NegotiateBufferSize(8192, 16384)
	if err != nil {
		return err
	}

	if err = dev.Prepare(); err != nil {
		return err
	}

	fmt.Printf("Negotiated parameters: %d channels, %d hz, %v, %d frame buffer, %d bytes/frame\n",
		channels, rate, format, bufferSize, dev.BytesPerFrame())

	var conn net.Conn // not used

	recBytes := rate * dev.BytesPerFrame() // 1 second's worth
	buf := make([]byte, recBytes)
	clip := make([]byte, recBytes*60)
	clipCount = 0
	clipSize := 0
	seconds := 0

	// NB: We record audio then write it synchronously
	// We should probably instead record in one go routine and write in another
	for {
		err = dev.Read(buf)
		if err != nil {
			return err
		}
		fmt.Printf(".")
		copy(clip[clipSize:], buf)
		clipSize += recBytes
		seconds++
		if seconds%60 == 0 {
			clipCount++
			fmt.Printf("\n")
			if err = sendClip(clip, output, conn); err != nil {
				return err
			}
			clipSize = 0
			seconds = 0
		}
	}
	return nil
}

// sendClipToFile writes a video/audio clip to a /tmp file.
func sendClipToFile(clip []byte, _ string, _ net.Conn) error {
	filename := fmt.Sprintf("/tmp/revid.%03d", clipCount)
	fmt.Printf("Writing %s (%d bytes)\n", filename, len(clip))
	err := ioutil.WriteFile(filename, clip, 0644)
	if err != nil {
		return fmt.Errorf("Error writing file %s: %s", filename, err)
	}
	return nil
}

// sendClipToHTPP posts a video/audio clip via HTTP, using a new TCP connection each time.
func sendClipToHTTP(clip []byte, output string, _ net.Conn) error {
	url := output + strconv.Itoa(len(clip)) // NB: append the size to the output
	fmt.Printf("Posting %s (%d bytes)\n", url, len(clip))
	resp, err := http.Post(url, mimeType, bytes.NewReader(clip)) // lighter than NewBuffer
	if err != nil {
		return fmt.Errorf("Error posting to %s: %s", output, err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err == nil {
		fmt.Printf("%s\n", body)
	}
	return err
}

// sendClipToUDP sends a video clip over UDP.
func sendClipToUDP(clip []byte, _ string, conn net.Conn) error {
	size := udpPackets * mp2tPacketSize
	fmt.Printf("Sending %d UDP packets of size %d (%d bytes)\n", len(clip)/size, size, len(clip))
	for offset := 0; offset < len(clip); offset += size {
		pkt := clip[offset : offset+size]
		_, err := conn.Write(pkt)
		if err != nil {
			return fmt.Errorf("UDP write error %s. Is your player listening?", err)
		}
	}
	return nil
}

// sendClipToRTP sends a video clip over RTP.
func sendClipToRTP(clip []byte, _ string, conn net.Conn) error {
	size := rtpPackets * mp2tPacketSize
	fmt.Printf("Sending %d RTP packets of size %d (%d bytes)\n",
		len(clip)/size, size+rtpHeaderSize, len(clip))
	pkt := make([]byte, rtpHeaderSize+rtpPackets*mp2tPacketSize)
	for offset := 0; offset < len(clip); offset += size {
		rtpEncapsulate(clip[offset:offset+size], pkt)
		_, err := conn.Write(pkt)
		if err != nil {
			return fmt.Errorf("RTP write error %s. Is your player listening?", err)
		}
	}
	return nil
}

// sendClipToStdout dumps video stats to stdout.
// Really only useful for debugging.
func sendClipToStdout(clip []byte, _ string, _ net.Conn) error {
	fmt.Printf("Dumping clip (%d bytes)\n", len(clip))

	if *flags&dumpProgramInfo != 0 {
		return mp2tDumpProgram(clip)
	}

	packetCount := 0
	discontinuities := 0
	var cc int

	for offset := 0; offset < len(clip); offset += mp2tPacketSize {
		packetCount++
		pkt, err := packet.FromBytes(clip[offset : offset+mp2tPacketSize])
		pktPID, err := packet.Pid(pkt)
		if err != nil {
			return err
		}
		if pktPID != uint16(*selectedPID) {
			continue
		}

		if *flags&(dumpPacketHeader|dumpPacketPayload) != 0 {
			fmt.Printf("Packet #%d.%d\n", clipCount, packetCount)
		}

		hasPayload := pkt[3]&0x10 != 0
		if !hasPayload {
			continue // nothing to do
		}

		// extract interesting info from header
		tei := pkt[1] & 0x80 >> 7
		pusi := pkt[1] & 0x40 >> 6
		tp := pkt[1] & 0x20 >> 5
		tcs := pkt[3] & 0xc0 >> 6
		afc := pkt[3] & 0x30 >> 4
		cc = int(pkt[3] & 0xf)

		if dumpCC != -1 && cc != dumpCC {
			discontinuities++
			fmt.Printf("Warning: Packet #%d.%d continuity counter out of order! Got %d, expected %d.\n",
				clipCount, packetCount, cc, dumpCC)
		}
		dumpCC = (cc + 1) % 16

		if *flags&dumpPacketHeader != 0 {
			fmt.Printf("\t\tTEI=%d, PUSI=%d, TP=%d, TSC=%d, AFC=%d, CC=%d\n", tei, pusi, tp, tcs, afc, cc)
		}

		if afc == 3 {
			// adaptation field, followed by payload
			afl := adaptationfield.Length(pkt)
			if adaptationfield.HasPCR(pkt) {
				pcrBase, pcrExt, _ := mp2tGetPCR(pkt)
				if *flags&dumpPacketHeader != 0 {
					fmt.Printf("\t\tAFL=%d, PCRbase=%d, PCRext=%d\n", afl, pcrBase, pcrExt)
				}
				if pcrBase < dumpPCRBase {
					fmt.Printf("Warning: PCRbase went backwards!\n")
				}
				dumpPCRBase = pcrBase
			} else if *flags&dumpPacketHeader != 0 {
				fmt.Printf("\t\tAFL=%d\n", afl)
			}
		}
		if *flags&dumpPacketPayload != 0 {
			fmt.Printf("\t\tPayload=%x\n", *pkt)
		}

	}
	if *flags&dumpPacketStats != 0 {
		fmt.Printf("%d packets of size %d bytes (%d bytes, %d discontinuites)\n",
			packetCount, packet.PacketSize, packetCount*packet.PacketSize, discontinuities)
	}
	return nil
}

// mp2tDumpProgram dumps MPEG-TS Program Association Table (PAT) and Program Map Tables (PMT).
func mp2tDumpProgram(clip []byte) error {
	// NB: Comcast API requires a buffered reader
	reader := bufio.NewReader(bytes.NewReader(clip))

	_, err := packet.Sync(reader)
	if err != nil {
		return fmt.Errorf("Error reading sync byte: %s", err)
	}
	pat, err := psi.ReadPAT(reader)
	if err != nil {
		return fmt.Errorf("Error reading PAT: %s", err)
	}
	mp2tDumpPat(pat)

	var pmts []psi.PMT
	pm := pat.ProgramMap()
	for pn, pid := range pm {
		pmt, err := psi.ReadPMT(reader, pid)
		if err != nil {
			return fmt.Errorf("Error reading PMT: %s", err)
		}
		pmts = append(pmts, pmt)
		mp2tDumpPmt(pn, pmt)
	}
	return nil
}

func mp2tDumpPat(pat psi.PAT) {
	fmt.Printf("Pat\n")
	fmt.Printf("\tPMT PIDs %v\n", pat.ProgramMap())
	fmt.Printf("\tNumber of Programs %v\n", pat.NumPrograms())
}

func mp2tDumpPmt(pn uint16, pmt psi.PMT) {
	// pn = program number
	fmt.Printf("Program #%v PMT\n", pn)
	fmt.Printf("\tPIDs %v\n", pmt.Pids())
	fmt.Printf("\tElementary Streams")
	for _, es := range pmt.ElementaryStreams() {
		fmt.Printf("\t\tPID %v : StreamType %v\n", es.ElementaryPid(), es.StreamType())
		for _, d := range es.Descriptors() {
			fmt.Printf("\t\t\t%+v\n", d)
		}
	}
}

// mp2tFixContinuity fixes discontinous MPEG-TS continuity counts (CC)
func mp2tFixContinuity(pkt *packet.Packet, packetCount int, pid uint16) bool {
	hasPayload, err := packet.ContainsPayload(pkt)
	if err != nil {
		fmt.Printf("Warning: Packet #%d.%d bad.\n", clipCount, packetCount)
		return false
	}
	if !hasPayload {
		return false
	}
	if pktPID, _ := packet.Pid(pkt); pktPID != pid {
		return false
	}
	fixed := false
	// extract continuity counter from 2nd nibble of 4th byte of header
	cc := int(pkt[3] & 0xf)
	if expectCC == -1 {
		expectCC = cc
	} else if cc != expectCC {
		pkt[3] = pkt[3]&0xf0 | byte(expectCC&0xf)
		fixed = true
	}
	expectCC = (expectCC + 1) % 16
	return fixed
}

// Mp2tGetPCR extracts the Program Clock Reference (PCR) from an MPEG-TS packet (if any)
func mp2tGetPCR(pkt *packet.Packet) (uint64, uint32, bool) {
	if !adaptationfield.HasPCR(pkt) {
		return 0, 0, false
	}
	pcrBytes, _ := adaptationfield.PCR(pkt) // 6 bytes
	// first 33 bits are PCR base, next 6 bits are reserved, final 9 bits are PCR extension.
	pcrBase := uint64(binary.BigEndian.Uint32(pcrBytes[:4]))<<1 | uint64(pcrBytes[4]&0x80>>7)
	pcrExt := uint32(pcrBytes[4]&0x01)<<1 | uint32(pcrBytes[5])
	return pcrBase, pcrExt, true
}

// rtpEncapsulate encapsulates MPEG-TS packets within an RTP header,
// setting the payload type accordingly (to 33) and incrementing the RTP sequence number.
func rtpEncapsulate(mp2tPacket []byte, pkt []byte) {
	// RTP packet encapsulates the MP2T
	// first 12 bytes is the header
	// byte 0: version=2, padding=0, extension=0, cc=0
	pkt[0] = 0x80 // version (2)
	// byte 1: marker=0, pt = 33 (MP2T)
	pkt[1] = 33
	// bytes 2 & 3: sequence number
	binary.BigEndian.PutUint16(pkt[2:4], rtpSequenceNum)
	if rtpSequenceNum == ^uint16(0) {
		rtpSequenceNum = 0
	} else {
		rtpSequenceNum++
	}
	// bytes 4,5,6&7: timestamp
	timestamp := uint32(time.Now().UnixNano() / 1e6) // ms timestamp
	binary.BigEndian.PutUint32(pkt[4:8], timestamp)
	// bytes 8,9,10&11: SSRC
	binary.BigEndian.PutUint32(pkt[8:12], rtpSSRC)

	// payload follows
	copy(pkt[rtpHeaderSize:rtpHeaderSize+rtpPackets*mp2tPacketSize], mp2tPacket)
}

// audio routines
// getRecordingDevice returns the first audio input device found
func getRecordingDevice() (*alsa.Device, error) {
	cards, err := alsa.OpenCards()
	if err != nil {
		return nil, err
	}
	defer alsa.CloseCards(cards)

	// use the first recording device we find
	var dev *alsa.Device

	for _, card := range cards {
		devices, err := card.Devices()
		if err != nil {
			return nil, err
		}
		for _, device := range devices {
			if device.Type != alsa.PCM {
				continue
			}
			if device.Record && dev == nil {
				dev = device
			}
		}
	}

	if dev == nil {
		return nil, errors.New("No recording device found")
	}
	return dev, nil
}
